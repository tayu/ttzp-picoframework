work_space="/afs/cern.ch/work/t/tayu/LQ_UL2017"
store_space="/eos/user/t/tayu/LQ_ULtest_sample"

if [ ! -d $work_space ]; then
  mkdir -p $work_space
fi

cd $work_space

######
#LHE-GEN step
######

LHE_GEN="LHE-GEN"
LHE_GEN_CMSSW="CMSSW_10_6_20_patch1"

:<<!
#LHE-GEN store space
if [ ! -d $store_space/$LHE_GEN ]; then
  mkdir -p $store_space/$LHE_GEN
fi

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/CMSSW_10_6_20_patch1/src ]; then
  echo release CMSSW_10_6_20_patch1 already exists
else
  cd $work_space
  scram p CMSSW_10_6_20_patch1
fi

#fragment step
if [ ! -d $work_space/CMSSW_10_6_20_patch1/src/Configuration/GenProduction/python ]; then
   mkdir -p $work_space/CMSSW_10_6_20_patch1/src/Configuration/GenProduction/python
fi

cd $work_space/CMSSW_10_6_20_patch1/src/Configuration/GenProduction/python

echo "import FWCore.ParameterSet.Config as cms

externalLHEProducer = cms.EDProducer(\"ExternalLHEProducer\",
    args = cms.vstring('/afs/cern.ch/work/t/tayu/GenProductions/bin/MadGraph5_aMCatNLO/SingleVectorLQ_M500_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz'),
    nEvents = cms.untracked.uint32(1000),
    numberOfParameters = cms.uint32(1),
    outputFile = cms.string('cmsgrid_final.lhe'),
    scriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)



import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter(\"Pythia8HadronizerFilter\",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13000.),
    PythiaParameters = cms.PSet(
         pythia8CommonSettingsBlock,
         pythia8CP5SettingsBlock,
         pythia8aMCatNLOSettingsBlock,
         pythia8PSweightsSettingsBlock,
         processParameters = cms.vstring(
              'TimeShower:nPartonsInBorn = 4', #number of coloured particles (before resonance decays) in born matrix element
        
         ),
    parameterSets = cms.vstring('pythia8CommonSettings',
                                'pythia8CP5Settings',
                                'pythia8aMCatNLOSettings',
                                'pythia8PSweightsSettings',
                                'processParameters'
                               )
         )
)
ProductionFilterSequence = cms.Sequence(generator)
">&  EXO-RunIISummer20UL17wmLHEGS-02236-fragment.py


cd $work_space/CMSSW_10_6_20_patch1/src/

cmsenv

scram b

#LHE-GEN work space

if [ ! -d $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN ]; then 
  mkdir -p $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN
fi

cd $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN

for((i=1;i<1;i++)){
if [ ! -d $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/LHE_GEN_${i} ]; then 
  mkdir -p $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/LHE_GEN_${i}
fi

cd $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/LHE_GEN_${i}
cmsenv

if [ ! -d $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/LHE_GEN_${i}/output ]; then
  mkdir error output log
fi

seed=$(( $RANDOM % 100000 + 1 ))
cmsDriver.py Configuration/GenProduction/python/EXO-RunIISummer20UL17wmLHEGS-02236-fragment.py \
--beamspot Realistic25ns13TeVEarly2017Collision \
--fileout file:/eos/user/t/tayu/LQ_ULtest_sample/LHE-GEN/EXO-RunIISummer20UL17wmLHEGEN-02236_${i}.root \
--mc \
--eventcontent RAWSIM,LHE \
--datatier GEN,LHE \
--conditions 106X_mc2017_realistic_v6 \
--step LHE,GEN \
--era Run2_2017 \
--geometry DB:Extended \
--python_filename EXO-RunIISummer20UL17wmLHEGEN-02236_${i}_cfg.py \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--customise_commands process.RandomNumberGeneratorService.externalLHEProducer.initialSeed="int(${seed})" \
--no_exec \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
CMS_VERSION=\"CMSSW_10_6_20_patch1\"
#scramv1 project CMSSW \${CMS_VERSION}
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/${CMS_VERSION}/src/$LHE_GEN/LHE_GEN_${i}
#cmsenv
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmLHEGEN-02236_${i}_cfg.py\n
 
" >& EXO-RunIISummer20UL17wmLHEGEN-02236_${i}_cfg.sh

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh

" >& EXO-RunIISummer20UL17wmLHEGEN-02236_${i}.cfg

chmod u+x EXO*
}

cd $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN
if [ -f $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/Job_LHEGEN.sh ]; then 
  rm Job_LHEGEN.sh 
fi

echo -e "for((i=1;i<1;i++)){
cd $work_space/CMSSW_10_6_20_patch1/src/$LHE_GEN/LHE_GEN_\${i} 

condor_submit EXO-RunIISummer20UL17wmLHEGEN-02236_\${i}.cfg\n
}">& Job_LHEGEN.sh

chmod u+x Job_LHEGEN.sh
!

######
#SIM step
######

SIM="SIM"
SIM_CMSSW="CMSSW_10_6_17_patch1"
#SIM store space
if [ ! -d $store_space/$SIM ]; then
  mkdir -p $store_space/$SIM
fi

:<<!
#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$SIM_CMSSW/src ]; then
  echo release $SIM_CMSSW already exists
else
  cd $work_space
  scram p $SIM_CMSSW
fi

if [ ! -d $work_space/$SIM_CMSSW/src/$SIM ]; then 
  mkdir -p $work_space/$SIM_CMSSW/src/$SIM
fi

cd $work_space/$SIM_CMSSW/src/$SIM
cmsenv

if [ ! -d $work_space/$SIM_CMSSW/src/$SIM/output ]; then
  mkdir error output log
fi

for((j=1;j<101;j++)){
cmsDriver.py --mc \
--eventcontent RAWSIM \
--runUnscheduled \
--datatier GEN-SIM \
--conditions 106X_mc2017_realistic_v6 \
--beamspot Realistic25ns13TeVEarly2017Collision \
--step SIM \
--nThreads 1 \
--geometry DB:Extended \
--era Run2_2017 \
--no_exec \
--filein file:$store_space/$LHE_GEN/EXO-RunIISummer20UL17wmLHEGEN-02236_${j}.root \
--fileout file:$store_space/$SIM/EXO-RunIISummer20UL17wmSIM-02236_${j}.root \
--python_filename EXO-RunIISummer20UL17wmSIM-02236_${j}_cfg.py \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
CMS_VERSION=\"$SIM_CMSSW\"
#scramv1 project CMSSW \${CMS_VERSION}
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$SIM_CMSSW/src/$SIM/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmSIM-02236_${j}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmSIM-02236_${j}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmSIM-02236.cfg

chmod u+x EXO*
!

######
#DIGIPremixRAW step
######

RAW="RAW"
RAW_CMSSW="CMSSW_10_6_17_patch1"


:<<!
passwd=`cat /afs/cern.ch/user/t/tayu/private/vomsproxy`
echo $passwd | voms-proxy-init --valid 192:00 -voms cms -rfc
cp /tmp/x509up_u93266 ~/

#SIM store space
if [ ! -d $store_space/$RAW ]; then
  mkdir -p $store_space/$RAW
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$RAW_CMSSW/src ]; then
  echo release $RAW_CMSSW already exists
else
  cd $work_space
  scram p $RAW_CMSSW
fi

if [ ! -d $work_space/$RAW_CMSSW/src/$RAW ]; then 
  mkdir -p $work_space/$RAW_CMSSW/src/$RAW
fi

cd $work_space/$RAW_CMSSW/src/$RAW
cmsenv

if [ ! -d $work_space/$RAW_CMSSW/src/$RAW/output ]; then
  mkdir error output log
fi

for((k=1;k<101;k++)){
cmsDriver.py --mc \
--eventcontent PREMIXRAW \
--runUnscheduled \
--datatier GEN-SIM-DIGI \
--conditions 106X_mc2017_realistic_v6 \
--step DIGI,DATAMIX,L1,DIGI2RAW \
--procModifiers premix_stage2 \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--nThreads 1 \
--geometry DB:Extended \
--datamix PreMix \
--era Run2_2017  \
--filein file:$store_space/$SIM/EXO-RunIISummer20UL17wmSIM-02236_${k}.root \
--fileout file:$store_space/$RAW/EXO-RunIISummer20UL17wmRAW-02236_${k}.root \
--pileup_input "dbs:/Neutrino_E-10_gun/RunIISummer20ULPrePremix-UL17_106X_mc2017_realistic_v6-v3/PREMIX" \
--python_filename EXO-RunIISummer20UL17wmDIGI-02236_${k}_cfg.py \
--no_exec \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
CMS_VERSION=\"$RAW_CMSSW\"
#scramv1 project CMSSW \${CMS_VERSION}
export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$RAW_CMSSW/src/$RAW/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmDIGI-02236_${k}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmDIGI-02236_${k}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmRAW-02236.cfg

chmod u+x EXO*
!

######
#HLT step
######

HLT="HLT"
HLT_CMSSW="CMSSW_9_4_14_UL_patch1"

:<<!
#SIM store space
if [ ! -d $store_space/$HLT ]; then
  mkdir -p $store_space/$HLT
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
if [ -d $work_space/$HLT_CMSSW/src ]; then
  echo release $HLT_CMSSW already exists
else
  cd $work_space
  scram p $HLT_CMSSW
fi

if [ ! -d $work_space/$HLT_CMSSW/src/$HLT ]; then 
  mkdir -p $work_space/$HLT_CMSSW/src/$HLT
fi

cd $work_space/$HLT_CMSSW/src/$HLT
cmsenv

if [ ! -d $work_space/$HLT_CMSSW/src/$HLT/output ]; then
  mkdir error output log
fi

for((l=1;l<101;l++)){
cmsDriver.py --mc \
--eventcontent RAWSIM \
--datatier GEN-SIM-RAW \
--conditions 94X_mc2017_realistic_v15 \
--customise_commands 'process.source.bypassVersionCheck = cms.untracked.bool(True)' \
--step HLT:2e34v40 \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--geometry DB:Extended \
--era Run2_2017  \
--filein file:$store_space/$RAW/EXO-RunIISummer20UL17wmRAW-02236_${l}.root \
--fileout file:$store_space/$HLT/EXO-RunIISummer20UL17wmHLT-02236_${l}.root \
--python_filename EXO-RunIISummer20UL17wmHLT-02236_${l}_cfg.py \
--no_exec \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
CMS_VERSION=\"$HLT_CMSSW\"
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$HLT_CMSSW/src/$HLT/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmHLT-02236_${l}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmHLT-02236_${l}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmHLT-02236.cfg

chmod u+x EXO*
!

######
#RECO step
######

RECO="RECO"
RECO_CMSSW="CMSSW_10_6_17_patch1"

:<<!
#SIM store space
if [ ! -d $store_space/$RECO ]; then
  mkdir -p $store_space/$RECO
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$RECO_CMSSW/src ]; then
  echo release $RECO_CMSSW already exists
else
  cd $work_space
  scram p $RECO_CMSSW
fi

if [ ! -d $work_space/$RECO_CMSSW/src/$RECO ]; then 
  mkdir -p $work_space/$RECO_CMSSW/src/$RECO
fi

cd $work_space/$RECO_CMSSW/src/$RECO
cmsenv

if [ ! -d $work_space/$RECO_CMSSW/src/$RECO/output ]; then
  mkdir error output log
fi

for((m=1;m<101;m++)){
cmsDriver.py --mc \
--eventcontent AODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--runUnscheduled \
--datatier AODSIM \
--conditions 106X_mc2017_realistic_v6 \
--step RAW2DIGI,L1Reco,RECO,RECOSIM \
--geometry DB:Extended \
--era Run2_2017 \
--filein file:$store_space/$HLT/EXO-RunIISummer20UL17wmHLT-02236_${m}.root \
--fileout file:$store_space/$RECO/EXO-RunIISummer20UL17wmRECO-02236_${m}.root \
--python_filename EXO-RunIISummer20UL17wmRECO-02236_${m}_cfg.py \
--no_exec \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
CMS_VERSION=\"$RECO_CMSSW\"
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$RECO_CMSSW/src/$RECO/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmRECO-02236_${m}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmRECO-02236_${m}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
requirements = (OpSysAndVer =?= \"CentOS7\")
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmRECO-02236.cfg

chmod u+x EXO*
!

######
#MINIAOD step
######

MINIAOD="MINIAOD"
MINIAOD_CMSSW="CMSSW_10_6_20"

:<<!

#SIM store space
if [ ! -d $store_space/$MINIAOD ]; then
  mkdir -p $store_space/$MINIAOD
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$MINIAOD_CMSSW/src ]; then
  echo release $MINIAOD_CMSSW already exists
else
  cd $work_space
  scram p $MINIAOD_CMSSW
fi

if [ ! -d $work_space/$MINIAOD_CMSSW/src/$MINIAOD ]; then 
  mkdir -p $work_space/$MINIAOD_CMSSW/src/$MINIAOD
fi

cd $work_space/$MINIAOD_CMSSW/src/$MINIAOD
cmsenv

if [ ! -d $work_space/$MINIAOD_CMSSW/src/$MINIAOD/output ]; then
  mkdir error output log
fi

for((n=1;n<101;n++)){
cmsDriver.py --eventcontent MINIAODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--datatier MINIAODSIM \
--conditions 106X_mc2017_realistic_v9 \
--step PAT \
--procModifiers run2_miniAOD_UL \
--geometry DB:Extended \
--filein file:$store_space/$RECO/EXO-RunIISummer20UL17wmRECO-02236_${n}.root \
--fileout file:$store_space/$MINIAOD/EXO-RunIISummer20UL17wmMINIAOD-02236_${n}.root \
--python_filename EXO-RunIISummer20UL17wmMINIAOD-02236_${n}_cfg.py \
--era Run2_2017 \
--runUnscheduled \
--no_exec \
--mc \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
CMS_VERSION=\"$MINIAOD_CMSSW\"
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$MINIAOD_CMSSW/src/$MINIAOD/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmMINIAOD-02236_${n}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmMINIAOD-02236_${n}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
requirements = (OpSysAndVer =?= \"CentOS7\")
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmMINIAOD-02236.cfg

chmod u+x EXO*

!

######
#NanoAOD step
######

NanoAOD="NanoAOD"
NanoAOD_CMSSW="CMSSW_10_6_26"
#SIM store space
if [ ! -d $store_space/$NanoAOD ]; then
  mkdir -p $store_space/$NanoAOD
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$NanoAOD_CMSSW/src ]; then
  echo release $NanoAOD_CMSSW already exists
else
  cd $work_space
  scram p $NanoAOD_CMSSW
fi

if [ ! -d $work_space/$NanoAOD_CMSSW/src/$NanoAOD ]; then 
  mkdir -p $work_space/$NanoAOD_CMSSW/src/$NanoAOD
fi

cd $work_space/$NanoAOD_CMSSW/src/$NanoAOD
cmsenv

if [ ! -d $work_space/$NanoAOD_CMSSW/src/$NanoAOD/output ]; then
  mkdir error output log
fi

for((g=1;g<101;g++)){
cmsDriver.py --eventcontent NANOAODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--datatier NANOAODSIM \
--conditions 106X_mc2017_realistic_v9 \
--step NANO \
--filein file:$store_space/$MINIAOD/EXO-RunIISummer20UL17wmMINIAOD-02236_${g}.root \
--fileout file:$store_space/$NanoAOD/EXO-RunIISummer20UL17wmNanoAODv9-02236_${g}.root \
--python_filename EXO-RunIISummer20UL17wmNanoAODv9-02236_${g}_cfg.py \
--era Run2_2017,run2_nanoAOD_106Xv2 \
--no_exec \
--mc \
-n 1000

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
#CMS_VERSION=\"$NanoAOD_CMSSW\"
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
export HOME=/afs/cern.ch/user/t/tayu
cd /afs/cern.ch/work/t/tayu/LQ_UL2017/$NanoAOD_CMSSW/src/$NanoAOD/
eval \`scram runtime -sh\`

cmsRun EXO-RunIISummer20UL17wmNanoAODv9-02236_${g}_cfg.py\n 
" >& EXO-RunIISummer20UL17wmNanoAODv9-02236_${g}_cfg.sh
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& EXO-RunIISummer20UL17wmNanoAOD-02236.cfg

chmod u+x EXO*
