model="Top_Philic_UFO"
work_space="/afs/cern.ch/work/t/tayu/Zprime_UL2017_${model}"
store_space="/eos/user/t/tayu/Zprime_UL2017sample_${model}"

CAMPAIGN="EXO-RunIISummer20UL17"
process="ttZprime"
parameter="theta1_ct1"
Mass="350"
gridpack="/eos/user/t/tayu/ttZprime/GenProductions/bin/MadGraph5_aMCatNLO/ttZprime_theta0_ct1_M350_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz"

nEvents=100
nJobs=10

LHE_GEN="LHEGEN"
SIM="SIM"
RAW="RAW"
HLT="HLT"
RECO="RECO"
MINIAOD="MINIAOD"
NanoAOD="NanoAOD"

LHE_GEN_CMSSW="${LHE_GEN}__CMSSW_10_6_20_patch1"
SIM_CMSSW="${SIM}__CMSSW_10_6_17_patch1"
RAW_CMSSW="DIGIPremix__CMSSW_10_6_17_patch1"
HLT_CMSSW="${HLT}__CMSSW_9_4_14_UL_patch1"
RECO_CMSSW="${RECO}__CMSSW_10_6_17_patch1"
MINIAOD_CMSSW="${MINIAOD}__CMSSW_10_6_20"
NanoAOD_CMSSW="${NanoAOD}__CMSSW_10_6_26"


if [ ! -d $work_space ]; then
  mkdir -p $work_space
fi

cd $work_space

######
#LHE-GEN step
######


:<<!
#LHE-GEN store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$LHE_GEN ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$LHE_GEN
fi

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$LHE_GEN_CMSSW/src ]; then
  echo release CMSSW_10_6_20_patch1 already exists
else
  cd $work_space
  scram p -n $LHE_GEN_CMSSW CMSSW CMSSW_10_6_20_patch1
fi

#fragment step
if [ ! -d $work_space/$LHE_GEN_CMSSW/src/Configuration/GenProduction/python ]; then
   mkdir -p $work_space/$LHE_GEN_CMSSW/src/Configuration/GenProduction/python
fi

cd $work_space/$LHE_GEN_CMSSW/src/Configuration/GenProduction/python

echo "import FWCore.ParameterSet.Config as cms

externalLHEProducer = cms.EDProducer(\"ExternalLHEProducer\",
    args = cms.vstring('$gridpack'),
    nEvents = cms.untracked.uint32(1000),
    numberOfParameters = cms.uint32(1),
    outputFile = cms.string('cmsgrid_final.lhe'),
    scriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)

import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter(\"Pythia8HadronizerFilter\",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13000.),
    PythiaParameters = cms.PSet(
         pythia8CommonSettingsBlock,
         pythia8CP5SettingsBlock,
         pythia8aMCatNLOSettingsBlock,
         pythia8PSweightsSettingsBlock,
         processParameters = cms.vstring(
              'TimeShower:nPartonsInBorn = 4', #number of coloured particles (before resonance decays) in born matrix element
        
         ),
    parameterSets = cms.vstring('pythia8CommonSettings',
                                'pythia8CP5Settings',
                                'pythia8aMCatNLOSettings',
                                'pythia8PSweightsSettings',
                                'processParameters'
                               )
         )
)
ProductionFilterSequence = cms.Sequence(generator)
">&  ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}-fragment.py


cd $work_space/$LHE_GEN_CMSSW/src/

cmsenv

scram b

#LHE-GEN work space

cd $work_space/$LHE_GEN_CMSSW/src/

for((i=0;i<${nJobs};i++)){
if [ ! -d $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_${i} ]; then 
  mkdir -p $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_${i}
fi

cd $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_${i}
cmsenv

if [ ! -d $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_${i}/output ]; then
  mkdir error output log
fi

seed=$(( $RANDOM % 100000 + 1 ))
cmsDriver.py Configuration/GenProduction/python/${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}-fragment.py \
--beamspot Realistic25ns13TeVEarly2017Collision \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$LHE_GEN/${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${i}.root \
--mc \
--eventcontent RAWSIM,LHE \
--datatier GEN,LHE \
--conditions 106X_mc2017_realistic_v6 \
--step LHE,GEN \
--era Run2_2017 \
--geometry DB:Extended \
--python_filename ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${i}_cfg.py \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--customise_commands process.RandomNumberGeneratorService.externalLHEProducer.initialSeed="int(${seed})" \
--no_exec \
-n $nEvents

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_${i}
#cmsenv
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${i}_cfg.py\n
 
" >& ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${i}_cfg.sh

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh

" >& ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${i}.cfg

chmod u+x EXO*
}

cd $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/
if [ -f $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/Job_LHEGEN.sh ]; then 
  rm Job_LHEGEN.sh 
fi

echo -e "for((i=0;i<${nJobs};i++)){
cd $work_space/$LHE_GEN_CMSSW/src/${process}_${parameter}/M${Mass}/LHE_GEN_\${i} 

condor_submit ${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_\${i}.cfg\n
}">& Job_LHEGEN.sh

echo -e "grep \"Exception\" LHEGEN*/error/*err\n
">& findbug.sh

chmod u+x Job_LHEGEN.sh

echo "step LHEGEN DONE !"

!

######
#SIM step
######

#SIM store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$SIM ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$SIM
fi

#SIM work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$SIM_CMSSW/src ]; then
  echo release $SIM_CMSSW already exists
else
  cd $work_space
  scram p -n $SIM_CMSSW CMSSW CMSSW_10_6_17_patch1
fi

if [ ! -d $work_space/$SIM_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$SIM_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$SIM_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$SIM_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((j=0;j<${nJobs};j++)){

if [ $j -eq 0 ]; then

cmsDriver.py --mc \
--eventcontent RAWSIM \
--runUnscheduled \
--datatier GEN-SIM \
--conditions 106X_mc2017_realistic_v6 \
--beamspot Realistic25ns13TeVEarly2017Collision \
--step SIM \
--nThreads 1 \
--geometry DB:Extended \
--era Run2_2017 \
--no_exec \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$LHE_GEN/${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${j}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$SIM/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}.root \
--python_filename ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py \
-n $nEvents

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd $work_space/$SIM_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py\n 
" >& ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.sh

fi

if [ $j -ne 0 ]; then

cp ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py

sed -i "s/${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmLHEGEN-${process}_${parameter}_M${Mass}_${j}.root/g" ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py

sed -i "s/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}.root/g" ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py

cp ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.sh

sed -i "s/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.py/g" ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${j}_cfg.sh 

fi

}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}.cfg

echo -e "grep \"Exception\" error/*err\n
">& findbug.sh

chmod u+x EXO*

echo "step SIM DONE !"
######
#DIGIPremixRAW step
######

passwd=`cat /afs/cern.ch/user/t/tayu/private/vomsproxy`
echo $passwd | voms-proxy-init --valid 192:00 -voms cms -rfc
cp /tmp/x509up_u93266 ~/

#RAW store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$RAW ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$RAW
fi

#RAW work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$RAW_CMSSW/src ]; then
  echo release $RAW_CMSSW already exists
else
  cd $work_space
  scram p -n $RAW_CMSSW CMSSW CMSSW_10_6_17_patch1
fi

if [ ! -d $work_space/$RAW_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$RAW_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$RAW_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$RAW_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((k=0;k<${nJobs};k++)){

if [ $k -eq 0 ]; then

cmsDriver.py --mc \
--eventcontent PREMIXRAW \
--runUnscheduled \
--datatier GEN-SIM-DIGI \
--conditions 106X_mc2017_realistic_v6 \
--step DIGI,DATAMIX,L1,DIGI2RAW \
--procModifiers premix_stage2 \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--nThreads 1 \
--geometry DB:Extended \
--datamix PreMix \
--era Run2_2017  \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$SIM/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${k}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$RAW/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}.root \
--pileup_input "dbs:/Neutrino_E-10_gun/RunIISummer20ULPrePremix-UL17_106X_mc2017_realistic_v6-v3/PREMIX" \
--python_filename ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py \
--no_exec \
-n ${nEvents}

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd $work_space/$RAW_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py\n 
" >& ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.sh

fi

if [ $k -ne 0 ]; then

cp ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py

sed -i "s/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmSIM-${process}_${parameter}_M${Mass}_${k}.root/g" ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py

sed -i "s/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}.root/g" ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py

cp ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.sh

sed -i "s/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.py/g" ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${k}_cfg.sh

fi
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}.cfg

echo -e "grep \"Exception\" error/*err\n
">& findbug.sh

chmod u+x EXO*

echo "step RAW DONE !"

######
#HLT step
######

#HLT store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$HLT ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$HLT
fi

#HLT work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
if [ -d $work_space/$HLT_CMSSW/src ]; then
  echo release $HLT_CMSSW already exists
else
  cd $work_space
  scram p -n $HLT_CMSSW CMSSW CMSSW_9_4_14_UL_patch1
fi

if [ ! -d $work_space/$HLT_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$HLT_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$HLT_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$HLT_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((l=0;l<${nJobs};l++)){

if [ $l -eq 0 ]; then

cmsDriver.py --mc \
--eventcontent RAWSIM \
--datatier GEN-SIM-RAW \
--conditions 94X_mc2017_realistic_v15 \
--customise_commands 'process.source.bypassVersionCheck = cms.untracked.bool(True)' \
--step HLT:2e34v40 \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--geometry DB:Extended \
--era Run2_2017  \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$RAW/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${l}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$HLT/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}.root \
--python_filename ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py \
--no_exec \
-n $nEvents

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
cd $work_space/$HLT_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py\n 
" >& ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.sh

fi

if [ $l -ne 0 ]; then

cp ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py

sed -i "s/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmRAW-${process}_${parameter}_M${Mass}_${l}.root/g" ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py

sed -i "s/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}.root/g" ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py

cp ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.sh

sed -i "s/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.py/g" ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${l}_cfg.sh

fi
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}.cfg

echo -e "grep \"exception\" error/*err\n
">& findbug.sh

chmod u+x EXO*

echo "step HLT DONE !"

######
#RECO step
######

#RECO store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$RECO ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$RECO
fi

#RECO work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$RECO_CMSSW/src ]; then
  echo release $RECO_CMSSW already exists
else
  cd $work_space
  scram p -n $RECO_CMSSW CMSSW CMSSW_10_6_17_patch1
fi

if [ ! -d $work_space/$RECO_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$RECO_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$RECO_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$RECO_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((m=0;m<${nJobs};m++)){

if [ $m -eq 0 ]; then

cmsDriver.py --mc \
--eventcontent AODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--runUnscheduled \
--datatier AODSIM \
--conditions 106X_mc2017_realistic_v6 \
--step RAW2DIGI,L1Reco,RECO,RECOSIM \
--geometry DB:Extended \
--era Run2_2017 \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$HLT/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${m}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$RECO/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}.root \
--python_filename ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py \
--no_exec \
-n $nEvents

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
cd $work_space/$RECO_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py\n 
" >& ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.sh

fi

if [ $m -ne 0 ]; then

cp ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py

sed -i "s/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmHLT-${process}_${parameter}_M${Mass}_${m}.root/g" ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py

sed -i "s/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}.root/g" ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py

cp ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.sh

sed -i "s/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.py/g" ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${m}_cfg.sh

fi
}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
requirements = (OpSysAndVer =?= \"CentOS7\")
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}.cfg

echo -e "grep \"exception\" error/*err\n
">& findbug.sh

chmod u+x EXO*

echo "step RECO DONE !"
######
#MINIAOD step
######

#MINIAOD store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$MINIAOD ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$MINIAOD
fi

#MINIAOD work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass} ]; then
  echo release $MINIAOD_CMSSW already exists
else
  cd $work_space
  scram p -n $MINIAOD_CMSSW CMSSW CMSSW_10_6_20
fi

if [ ! -d $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((n=0;n<${nJobs};n++)){

if [ $n -eq 0 ]; then

cmsDriver.py --eventcontent MINIAODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--datatier MINIAODSIM \
--conditions 106X_mc2017_realistic_v9 \
--step PAT \
--procModifiers run2_miniAOD_UL \
--geometry DB:Extended \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$RECO/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${n}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$MINIAOD/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}.root \
--python_filename ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py \
--era Run2_2017 \
--runUnscheduled \
--no_exec \
--mc \
-n $nEvents

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
#export X509_USER_PROXY=/afs/cern.ch/user/t/tayu/x509up_u93266
cd $work_space/$MINIAOD_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py\n 
" >& ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.sh

fi

if [ $n -ne 0 ]; then

cp ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py

sed -i "s/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmRECO-${process}_${parameter}_M${Mass}_${n}.root/g" ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py

sed -i "s/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}.root/g" ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py

cp ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.sh

sed -i "s/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.py/g" ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${n}_cfg.sh

fi

}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+AccountingGroup = \"group_u_CMS.CAF.COMM\"
+MaxRuntime = 60*60*24
request_cpus = 1
requirements = (OpSysAndVer =?= \"CentOS7\")
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}.cfg

chmod u+x EXO*

echo -e "grep \"Exception\" error/*err\n
">& findbug.sh

echo "step MINIAOD DONE !"

######
#NanoAOD step
######

#NanoAOD store space
if [ ! -d $store_space/${process}_${parameter}/M${Mass}/$NanoAOD ]; then
  mkdir -p $store_space/${process}_${parameter}/M${Mass}/$NanoAOD
fi

#NanoAOD work space
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
if [ -d $work_space/$NanoAOD_CMSSW/src ]; then
  echo release $NanoAOD_CMSSW already exists
else
  cd $work_space
  scram p -n $NanoAOD_CMSSW CMSSW CMSSW_10_6_26
fi

if [ ! -d $work_space/$NanoAOD_CMSSW/src/${process}_${parameter}/M${Mass} ]; then 
  mkdir -p $work_space/$NanoAOD_CMSSW/src/${process}_${parameter}/M${Mass}
fi

cd $work_space/$NanoAOD_CMSSW/src/${process}_${parameter}/M${Mass}
cmsenv

if [ ! -d $work_space/$NanoAOD_CMSSW/src/${process}_${parameter}/M${Mass}/output ]; then
  mkdir error output log
fi

for((g=0;g<${nJobs};g++)){

if [ $g -eq 0 ]; then
cmsDriver.py --eventcontent NANOAODSIM \
--customise Configuration/DataProcessing/Utils.addMonitoring \
--datatier NANOAODSIM \
--conditions 106X_mc2017_realistic_v9 \
--step NANO \
--filein file:$store_space/${process}_${parameter}/M${Mass}/$MINIAOD/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${g}.root \
--fileout file:$store_space/${process}_${parameter}/M${Mass}/$NanoAOD/${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}.root \
--python_filename ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py \
--era Run2_2017,run2_nanoAOD_106Xv2 \
--no_exec \
--mc \
-n ${nEvents}

echo -e "#!/bin/bash

echo \"Starting job on \" \`date\` #Date/time of start of job
echo \"Runing on: \`uname -a\`\" #Condor job is running on this node
echo \"System software: \`cat /etc/redhat-release\`\" #Operating System on that node

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
export HOME=/afs/cern.ch/user/t/tayu
cd $work_space/$NanoAOD_CMSSW/src/${process}_${parameter}/M${Mass}
eval \`scram runtime -sh\`

cmsRun ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py\n 
" >& ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.sh
fi


if [ $g -ne 0 ]; then

cp ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_0_cfg.py ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py

sed -i "s/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmMINIAOD-${process}_${parameter}_M${Mass}_${g}.root/g" ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py

sed -i "s/${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_0.root/${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}.root/g" ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py

cp ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_0_cfg.sh ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.sh

sed -i "s/${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_0_cfg.py/${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.py/g" ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}_${g}_cfg.sh

fi

}

echo "universe = vanilla
executable = \$(filename)
output = output/\$Fn(filename).out
error = error/\$Fn(filename).err
log = log/\$Fn(filename).log
should_transfer_files   = No
+MaxRuntime = 60*60*24
request_cpus = 1
queue filename matching files EXO*.sh
" >& ${CAMPAIGN}wmNanoAOD-${process}_${parameter}_M${Mass}.cfg

chmod u+x EXO*

echo -e "grep \"Exception\" error/*err\n
">& findbug.sh

echo "step NanoAOD DONE !"

cd $work_space
